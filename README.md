# User list with Laravel + Vuejs

This is a simple application to display a list of users using Laravel in the backend, and Vuejs in the front.

### Configure your .env:
```sh
DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE={YOUR_NAME_DATABASE}
DB_USERNAME={YOUR_USERNAME}
DB_PASSWORD={YOUR_PASSWORD}

```

### Run the migrates and seed:
```sh
$ php artisan migrate --seed
```

### API Resources


| Method | URI | Description |
| ------ | ------ | ------ |
| GET | [/api/users](#get-users) | Get list of users |

#### User fields:

| Name | Type | Description |
| ------ | ------ | ------ |
| id | integer | User id |
| first_name | string | First user name |
| last_name | string | Last user name |
| email | string | User email |
| created_at | datetime | Creation date |
| updated_at | datetime | Last Updated time |


### GET /api/users

Example: /api/users

Response body Success:

Status Code: 200

     [
       {
         "id": 5,
         "first_name": "Bertha Rutherford",
         "last_name": "Ms. Cordie Koss DVM",
         "email": "sammy61@example.net",
         "created_at": "2021-05-30T18:46:36.000000Z",
         "updated_at": "2021-05-30T18:46:36.000000Z"
       },
       {
         "id": 9,
         "first_name": "Dortha Schoen",
         "last_name": "Nils Grimes",
         "email": "gerhold.rafael@example.net",
         "created_at": "2021-05-31T11:32:28.000000Z",
         "updated_at": "2021-05-31T11:32:28.000000Z"
       }
     ]

### Access the frontend
/users

![Image of User List](https://drive.google.com/file/d/1phuq4QGtgkyXbURDRDGa7-_P9h_w8rzL/view?usp=sharing)