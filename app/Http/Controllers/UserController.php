<?php

namespace App\Http\Controllers;

use App\Services\UserService;
use Illuminate\Http\Request;

class UserController extends Controller
{
    private $userService;

    /**
     * Constructor class
     *
     * @access public
     * @param UserService $userService
     * @return void
     */
    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    function index()
    {
        return view('users');
    }


    function getUsers(Request $request)
    {
        $data = $this->userService->getUsers($request->input('sort', 'first_name'));
        return response()->json($data);
    }
}