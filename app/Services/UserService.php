<?php


namespace App\Services;


use App\Http\Resources\UserResource;
use App\Models\User;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class UserService
{

    /**
     * @var User
     */
    public $user;

    /**
     * InvitationService constructor.
     * @param User $user
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * @param string $sort
     * @return AnonymousResourceCollection
     */
    public function getUsers(string $sort = "first_name"): AnonymousResourceCollection
    {
        return UserResource::collection($this->user->orderBy($sort)->get());
    }
}