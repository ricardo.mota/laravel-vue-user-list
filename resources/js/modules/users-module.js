import axios from 'axios'

const state = {
  users: []
};

const getters = {
  usersList: state => state.users
};

const actions = {
  async fetchUsers({commit}){
    const response = await axios.get("api/users");
    commit("setUsers", response.data)
  },
};

const mutations = {
  setUsers: (state, users) => (
    state.users = users
  ),
};

export default {
  state,
  getters,
  actions,
  mutations
}