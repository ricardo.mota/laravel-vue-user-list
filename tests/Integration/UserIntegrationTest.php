<?php

namespace Tests\Integration;

use App\Models\User;
use Tests\Setup;

class UserIntegrationTest extends Setup
{
    /**
     * structure user const
     */
    protected const STRUCTUREUSER = [
        'id',
        'first_name',
        'last_name',
        'email',
        'created_at',
        'updated_at'
    ];

    public $users;

    public function setUp(): void
    {
        parent::setUp();
        $this->users = User::factory()->count(5)->create();
    }

    public function testGetUsersList()
    {
        $response = $this->get("/api/users");

        $response->assertStatus(200);
        $response->assertJsonStructure([self::STRUCTUREUSER]);
        $response->assertJsonFragment(['first_name' => $this->users->toArray()[0]['first_name']]);
        $response->assertJsonFragment(['last_name' => $this->users->toArray()[0]['last_name']]);
    }
}