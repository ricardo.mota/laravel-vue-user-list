<?php


namespace Tests\Unit;

use App\Models\User;
use App\Services\UserService;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Tests\Setup;

class UserUnitTest extends Setup
{

    public function setUp(): void
    {
        parent::setUp();
        User::factory()->count(5)->create();
    }

    public function testGetUserService()
    {
        $user = new User();
        $service = new UserService($user);
        $this->assertInstanceOf(AnonymousResourceCollection::class, $service->getUsers());
    }
}